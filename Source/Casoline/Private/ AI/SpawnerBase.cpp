// Fill out your copyright notice in the Description page of Project Settings.

#include " AI/SpawnerBase.h"
#include " AI/AICharacter.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ASpawnerBase::ASpawnerBase()
{
  // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ASpawnerBase::BeginPlay()
{
  Super::BeginPlay();

  WaveTimer(0);
  OnWave.AddDynamic(this, &ASpawnerBase::WaveTimer);
}

void ASpawnerBase::WaveTimer(int32 myWave)
{
  if (myWave > 2)
    return;
  if (myWave == 2)
  {
    MaxMob1 = 0;
    MaxMob2 = 0;
    MaxMob3 = 1;
  }
  if (myWave < 2)
  {
    MaxMob1 = MaxMobsDefault;
    MaxMob2 = MaxMobsDefault;
    MaxMob3 = MaxMobsDefault;
  }

  GetWorldTimerManager().SetTimer(SpawnHandle, this, &ASpawnerBase::SpawnBots, 1.0f, true, 10.0f);
}

// Called every frame
void ASpawnerBase::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);
}

void ASpawnerBase::SpawnBots()
{
  if (MaxMob1 > 0 || MaxMob2 > 0 || MaxMob3 > 0)
  {
    int32 NumPoint = 0;
    FVector L_SpawnPoint;
    switch (UKismetMathLibrary::RandomInteger(3))
    {
    case 0:
      NumPoint = 0;
      L_SpawnPoint = SpawnLoc1;
      if (MaxMob1 > 0)
        break;
    case 1:
      NumPoint = 1;
      L_SpawnPoint = SpawnLoc2;
      if (MaxMob2 > 0)
        break;
    case 2:
      NumPoint = 2;
      L_SpawnPoint = SpawnLoc3;
      if (MaxMob3 > 0)
        break;
    }
TSubclassOf<AAICharacter> L_Bot;
    switch (Wave)
    {
    case 0:
      L_Bot = Bot1;
      break;
    case 1:
      L_Bot = FMath::RandBool() ? Bot1 : Bot2;
      break;
    case 2:
      L_Bot = Bot3;
      break;
    }
    
    SpawnOneBot(L_Bot, L_SpawnPoint);
    switch (NumPoint)
    {
    case 0:
      --MaxMob1;
      break;
    case 1:
      --MaxMob2;
      break;
    case 2:
      --MaxMob3;
      break;
    }
    /*if (Wave >= 2)
    {
      MaxMob1 = -1;
      MaxMob2 = -1;
      MaxMob3 = -1;
    }*/
  }
  else
  {
    Wave = Wave + 1;
    if (SpawnHandle.IsValid())
      GetWorldTimerManager().ClearTimer(SpawnHandle);
    
    OnWave.Broadcast(Wave);
  }
}

void ASpawnerBase::SpawnOneBot(TSubclassOf<AAICharacter> Bot, FVector SpawnLocation)
{
  FTransform Params;
  Params.SetLocation(SpawnLocation);
  GetWorld()->SpawnActor<AAICharacter>(Bot, Params);
}
