// Fill out your copyright notice in the Description page of Project Settings.

#include " AI/AICharacter.h"
#include "Components/HealthComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Develop/CPlayer.h"

// Sets default values
AAICharacter::AAICharacter()
{
  // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = true;

  HealthComponent = CreateDefaultSubobject<UHealthComponent>("HealthComponent");
}

// Called when the game starts or when spawned
void AAICharacter::BeginPlay()
{
  Super::BeginPlay();
  check(GetWorld());

  HealthComponent->OnDeath.AddUObject(this, &AAICharacter::Death);

  GetWorldTimerManager().SetTimer(DamageHandle, this, &AAICharacter::DamageSphere, 1.f, true);

  GetMesh()->SetRelativeScale3D(Scale);
  GetCharacterMovement()->MaxWalkSpeed = Speed;
  HealthComponent->MaxHealth = HP;
}

// Called every frame
void AAICharacter::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AAICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
  Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AAICharacter::Death()
{
  GetCharacterMovement()->DisableMovement();
  SetLifeSpan(5.0f);

  GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
  if (GetMesh())
  {
    GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    GetMesh()->SetSimulatePhysics(true);
  }
  if (DamageHandle.IsValid())
  {
    GetWorldTimerManager().ClearTimer(DamageHandle);
  }
  if (Pickup)
  {
    if (FMath::RandRange(0.0f, 1.0f) < Veroyatnost)
    {
      AActor* myPickup = GetWorld()->SpawnActor<AActor>(Pickup);
      if (myPickup)
        myPickup->SetActorLocation(GetActorLocation());
    }
  }
}

void AAICharacter::DamageSphere()
{

  if (IsBoss)
  {                                                    // ������ ����� �����
    float BossDamage = PlayerInRadius ? 0.0f : Damage; // ���� ����� �� ���������� �������, ���� = 0
    UGameplayStatics::ApplyRadialDamage(GetWorld(),    //
        BossDamage,                                    //
        GetActorLocation(),                            //
        Radius2,                                       //
        nullptr,                                       //
        {this});
    UKismetSystemLibrary::DrawDebugSphere(GetWorld(), GetActorLocation(), Radius2, 12, //
        FLinearColor::Green, 0.5f, 2.0f);
  }
  else
  {                                                 // ������ ����� �������� ����
    UGameplayStatics::ApplyRadialDamage(GetWorld(), //
        Damage,                                     //
        GetActorLocation(),                         //
        Radius1,                                    //
        nullptr,                                    //
        {});
  }
  UKismetSystemLibrary::DrawDebugSphere(GetWorld(), GetActorLocation(), Radius1, 12, //
      FLinearColor::Red, 0.5f, 2.0f);
}
