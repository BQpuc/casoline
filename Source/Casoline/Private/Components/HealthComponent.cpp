// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/HealthComponent.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/Controller.h"
#include "Camera/CameraShakeBase.h"
#include "Develop/CGameInstance.h"
#include "Casoline/CasolineGameModeBase.h"

DEFINE_LOG_CATEGORY_STATIC(HealthLog, All, All)

UHealthComponent::UHealthComponent()
{
  PrimaryComponentTick.bCanEverTick = false;
}

void UHealthComponent::BeginPlay()
{
  Super::BeginPlay();

  Health = MaxHealth;
  AActor* Player = Cast<AActor>(GetOwner());
  Player->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::AnyDamage);
  UE_LOG(HealthLog, Display, TEXT("On Health component: %s"), *Player->GetName());
}

void UHealthComponent::AnyDamage(
    AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
  if (bIsDead)
    return;
  if (Health <= 0.0f)
  {
    return;
  }
  KillerContr = InstigatedBy;
  ChangeHealth(Damage);

  // Debug Update regeration and death delegate
  ChangeHealth(0.0f);
  PlayCameraShake();
}

void UHealthComponent::PlayCameraShake()
{
  if (bIsDead)
    return;
  APawn* Player = Cast<APawn>(GetOwner());
  if (!Player)
    return;
  APlayerController* Controller = Player->GetController<APlayerController>();
  if (!Controller || !Controller->PlayerCameraManager)
    return;

  Controller->PlayerCameraManager->StartCameraShake(CameraShake);
}

void UHealthComponent::ChangeHealth(float Damage)
{

  if (bIsDead)
  {
    UE_LOG(HealthLog, Display, TEXT("bIsDead: %d"), bIsDead);
    return;
  }
  if (Health <= 0.0f)
  {
    OnDeath.Broadcast();
    bIsDead = true;
  }
  // Regen Timer
  if (bAutoHeal)
  {
    if (Damage > 0.0f)
    {
      RegenTime = HealDelay;
    }
    // UE_LOG(HealthLog, Display, TEXT("RegenTime: %.2f Health: %0.2f bRegenFlag : % d"), RegenTime, Health, bRegenFlag);
    if (!bRegenFlag && (Health < MaxHealth))
    {
      bRegenFlag = true;
      GetWorld()->GetTimerManager().SetTimer(RegenHandle, this, &UHealthComponent::Regeneration, HealUpdateTime, true);

      // UE_LOG(HealthLog, Display, TEXT("=== REGENERATION BEGIN ==="));
    }
  }
  Health = FMath::Clamp(Health - Damage, 0.0f, MaxHealth);
  OnChangeHealth.Broadcast(Health, Damage);
}

bool UHealthComponent::RegenPercent(float Percent)
{
  if (Health >= MaxHealth || Percent <= 0.0f || bIsDead)
    return false;

  float LocPercent = FMath::Clamp(Percent, 0.0f, 1.0f);
  Health = FMath::Clamp(Health + MaxHealth * LocPercent, 0.0f, MaxHealth);
  return true;
}

void UHealthComponent::Regeneration()
{
  if (RegenTime <= 0.0f)
  {
    if (Health >= MaxHealth)
    {
      bRegenFlag = false;
      GetWorld()->GetTimerManager().ClearTimer(RegenHandle);
    }
    ChangeHealth(-HealRegen);
    // UE_LOG(HealthLog, Display, TEXT("Regeneration %.1f"), HealRegen);
  }
  else
  {
    if (bIsDead)
      GetWorld()->GetTimerManager().ClearTimer(RegenHandle);
    RegenTime -= HealUpdateTime;
    // UE_LOG(HealthLog, Display, TEXT("Regen Time: %.2f"), RegenTime);
  }
}