// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/PauseWidget.h"
#include "Gameframework/GameModeBase.h"
#include "Components/Button.h"

bool UPauseWidget::Initialize()
{// �������� �������� � ������, ����� ������ �� ���� �������
  bool InitStatus = Super::Initialize(); 
  if (ClearPauseButton)                  
  {// ������������� �� ������� ������� ������ ��� �����
    ClearPauseButton->OnClicked.AddDynamic(this, &UPauseWidget::OnClearPause);
  }
  return InitStatus;
}

void UPauseWidget::OnClearPause()
{
  if (!GetWorld() || !GetWorld()->GetAuthGameMode())
    return;
  GetWorld()->GetAuthGameMode()->ClearPause(); // ���������� ���� ���� �������
}