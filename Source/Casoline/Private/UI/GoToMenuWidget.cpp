// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/GoToMenuWidget.h"
#include "Components/Button.h"
#include "Develop/CGameInstance.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(ToMenuLog, All, All);

void UGoToMenuWidget::NativeOnInitialized()
{
  Super::NativeOnInitialized();

  if (GoToMenuButton)
  {
    GoToMenuButton->OnClicked.AddDynamic(this, &UGoToMenuWidget::OnGoToMenu);
  }
}

void UGoToMenuWidget::OnGoToMenu()
{
  if (!GetWorld())
    return;
  const auto GameInstance = GetWorld()->GetGameInstance<UCGameInstance>();
  if (!GameInstance)
    return;
  if (GameInstance->GetMenuLvlName().IsNone())
  {
    UE_LOG(ToMenuLog, Error, TEXT("MenuLvlMap in GameInstance is none! "));
    return;
  }
  UGameplayStatics::OpenLevel(this, GameInstance->GetMenuLvlName());
}