// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/GameHUD.h"
#include "Engine/Canvas.h"
#include "Casoline/CasolineGameModeBase.h"
#include "UI/GameWidget.h"



void AGameHUD::BeginPlay()
{
  Super::BeginPlay();

  // ������� ������� � ��������� � TMap<>
  GameWidgets.Add(EMatchState::InProgress, CreateWidget<UUserWidget>(GetWorld(), PlayerWidget));
  GameWidgets.Add(EMatchState::Pause, CreateWidget<UUserWidget>(GetWorld(), PauseWidget));
  GameWidgets.Add(EMatchState::GameOver, CreateWidget<UUserWidget>(GetWorld(), GameOverWidget));
  for (auto GameWidgetPair : GameWidgets)
  { // ��������� ������ ������ �� �������(�� �����)
    UUserWidget* GameWidget = GameWidgetPair.Value;
    if (!GameWidget)
      continue;
    GameWidget->AddToViewport();
    GameWidget->SetVisibility(ESlateVisibility::Hidden);
  }
  if (GetWorld())
  { // ������ ��������� ��������� ����
    const auto GameMode = Cast<ACasolineGameModeBase>(GetWorld()->GetAuthGameMode());
    if (GameMode)
    {
      GameMode->OnMathStateChanged.AddUObject(this, &AGameHUD::OnMatchStateChahged);
    }
  }
}

void AGameHUD::OnMatchStateChahged(EMatchState State)
{                    
  if (CurrentWidget) // ������ ��� ������ ������� � ��� �� ����������
  {
    CurrentWidget->SetVisibility(ESlateVisibility::Hidden); // �������� ������� ������
  }
  if (GameWidgets.Contains(State)) // ���������, ���������� �� � ������� ������� � ������ State
  {
    CurrentWidget = GameWidgets[State]; // ������������� �������� �������
  }
  if (CurrentWidget)
  {
    CurrentWidget->SetVisibility(ESlateVisibility::Visible); // ������ ������� ������� ������
  }
}
