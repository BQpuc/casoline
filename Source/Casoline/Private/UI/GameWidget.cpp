// Fill out your copyright notice in the Description page of Project Settings.

#include "UI/GameWidget.h"
#include "Casoline/CasolineGameModeBase.h"
#include "Develop/CPlayer.h"
#include "Weapon/Weapon.h"
#include "Components/HealthComponent.h"
#include " AI/SpawnerBase.h"

int32 UGameWidget::GetBullets() const
{
  ACPlayer* Player = Cast<ACPlayer>(GetOwningPlayer()->GetPawn());
  if (!Player)
    return 0;
  return Player->GetWeapon()->Bullets;
}

int32 UGameWidget::GetBulletsInClip() const
{
  ACPlayer* Player = Cast<ACPlayer>(GetOwningPlayer()->GetPawn());
  if (!Player)
    return 0;
  return Player->GetWeapon()->BulletsInClip;
}

int32 UGameWidget::GetClip() const
{
  ACPlayer* Player = Cast<ACPlayer>(GetOwningPlayer()->GetPawn());
  if (!Player)
    return 0;
  return Player->GetWeapon()->Clip;
}

int32 UGameWidget::GetWave() const
{
  ACPlayer* Player = Cast<ACPlayer>(GetOwningPlayer()->GetPawn());
  if (!Player)
    return 0;
  if (Player->Spawner)
  return Player->Spawner->Wave;
  return -1;
}

int32 UGameWidget::GetMoney() const
{
  ACPlayer* Player = Cast<ACPlayer>(GetOwningPlayer()->GetPawn());
  if (!Player)
    return 0;
  return Player->Money;
}

bool UGameWidget::GetInRadius() const
{
  ACPlayer* Player = Cast<ACPlayer>(GetOwningPlayer()->GetPawn());
  if (!Player)
    return false;
  return Player->PlayerInRadius;
}

bool UGameWidget::GetAuctionVisibility() const
{
  ACPlayer* Player = Cast<ACPlayer>(GetOwningPlayer()->GetPawn());
  if (!Player)
    return false;
  return Player->AuctionVisibility;
}

int32 UGameWidget::GetPrice() const
{
  ACPlayer* Player = Cast<ACPlayer>(GetOwningPlayer()->GetPawn());
  if (!Player)
    return 0;
  return Player->Price;
}

int32 UGameWidget::GetSellingAmmo() const
{
  ACPlayer* Player = Cast<ACPlayer>(GetOwningPlayer()->GetPawn());
  if (!Player)
    return 0;
  return Player->SellingAmmo;
}

float UGameWidget::GetHealthPercent() const
{
  ACPlayer* Player = Cast<ACPlayer>(GetOwningPlayer()->GetPawn());
  if (!Player)
    return 0.0f;
  UHealthComponent* HealthComp = UTypes::GetPlayerComponent<UHealthComponent>(Player);
  if (!HealthComp)
    return 0.0f;
  return HealthComp->GetHealthPercent();
}

