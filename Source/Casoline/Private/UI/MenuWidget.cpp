// Fill out your copyright notice in the Description page of Project Settings.

#include "UI/MenuWidget.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "Develop/CGameInstance.h"
#include "Kismet/KismetSystemLibrary.h"

DEFINE_LOG_CATEGORY_STATIC(MenuWidgetLog, All, All);

void UMenuWidget::NativeOnInitialized()
{
  Super::NativeOnInitialized();

  if (StartGameButton)
  {
    StartGameButton->OnClicked.AddDynamic(this, &UMenuWidget::OnStartGame);
  }
  if (QuitGameButton)
  {
    QuitGameButton->OnClicked.AddDynamic(this, &UMenuWidget::OnQuitGame);
  }
}

void UMenuWidget::OnStartGame()
{
  if (!GetWorld())
  {
    UE_LOG(MenuWidgetLog, Error, TEXT("World is not valid"));
    return;
  }

  const auto GameInstance = GetWorld()->GetGameInstance<UCGameInstance>();
  if (!GameInstance)
  {
    UE_LOG(MenuWidgetLog, Error, TEXT("GameInstance is not valid"));
    return;
  }
  if (GameInstance->GetStartupLvlName().IsNone())
  {
    UE_LOG(MenuWidgetLog, Error, TEXT("StartupLevelName is None"));
    return;
  }
  UGameplayStatics::OpenLevel(this, GameInstance->GetStartupLvlName());
}

void UMenuWidget::OnQuitGame()
{
  UKismetSystemLibrary::QuitGame(this, GetOwningPlayer(), EQuitPreference::Quit, true);
}