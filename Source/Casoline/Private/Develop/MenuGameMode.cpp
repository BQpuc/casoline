// Fill out your copyright notice in the Description page of Project Settings.


#include "Develop/MenuGameMode.h"
#include "Develop/MenuPlayerController.h"
#include "UI/MenuHUD.h"

AMenuGameMode::AMenuGameMode() 
{
  PlayerControllerClass = AMenuPlayerController::StaticClass();
  HUDClass = AMenuHUD::StaticClass();
}