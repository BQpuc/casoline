// Fill out your copyright notice in the Description page of Project Settings.

#include "Develop/CPlayer.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
//#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Develop/CGameInstance.h"
#include "Weapon/Weapon.h"
#include "Engine/World.h"
#include "Components/HealthComponent.h"
#include "Casoline/CasolineGameModeBase.h"
#include " AI/SpawnerBase.h"

// Sets default values
ACPlayer::ACPlayer()
{
  // Set size for player capsule
  GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

  // Don't rotate character to camera direction
  bUseControllerRotationPitch = false;
  bUseControllerRotationYaw = false;
  bUseControllerRotationRoll = false;

  // Configure character movement
  GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
  GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
  GetCharacterMovement()->bConstrainToPlane = true;
  GetCharacterMovement()->bSnapToPlaneAtStart = true;

  // Create a camera boom...
  CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
  CameraBoom->SetupAttachment(RootComponent);
  CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
  CameraBoom->TargetArmLength = 800.f;
  CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
  CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

  // Create a camera...
  TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
  TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
  TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

  // Activate ticking in order to update the cursor every frame.
  PrimaryActorTick.bCanEverTick = true;
  PrimaryActorTick.bStartWithTickEnabled = true;

  HealthComponent = CreateDefaultSubobject<UHealthComponent>("HealthComponent");
}

// Called when the game starts or when spawned
void ACPlayer::BeginPlay()
{
  Super::BeginPlay();
  check(HealthComponent);
  check(GetWorld());


  if (CursorMaterial)
  {
    CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
  }
  CurrentWeapon = GetWorld()->SpawnActor<AWeapon>(WeaponClass);
  if (CurrentWeapon)
  {
    FAttachmentTransformRules Attach(EAttachmentRule::SnapToTarget, false);
    CurrentWeapon->AttachToComponent(GetMesh(), Attach, WeaponSocketName);
    CurrentWeapon->OnReload.AddUObject(this, &ACPlayer::BeginReload);
  }
  HealthComponent->OnDeath.AddUObject(this, &ACPlayer::Death);
  HealthComponent->OnChangeHealth.AddUObject(this, &ACPlayer::IfHealthChanged);
  Spawner = GetWorld()->SpawnActor<ASpawnerBase>(SpawnerClass);
  NewAuction.AddUObject(this, &ACPlayer::SetAuctionTimer);
  SetAuctionTimer();
}

void ACPlayer::StartFire()
{
  if (CanFire())
  CurrentWeapon->StartFire();
}

void ACPlayer::StopFire()
{
  CurrentWeapon->StopFire();
}

// Called every frame
void ACPlayer::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);

  if (CurrentCursor)
  {
    APlayerController* myPC = Cast<APlayerController>(GetController());
    if (myPC)
    {
      FHitResult TraceHitResult;
      myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
      FVector CursorFV = TraceHitResult.ImpactNormal;
      FRotator CursorR = CursorFV.Rotation();

      CurrentCursor->SetWorldLocation(TraceHitResult.Location);
      CurrentCursor->SetWorldRotation(CursorR);
    }
  }

  MovementTick(DeltaTime);
}

// Called to bind functionality to input
void ACPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
  Super::SetupPlayerInputComponent(PlayerInputComponent);

  PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ACPlayer::InputAxisX);
  PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ACPlayer::InputAxisY);
  PlayerInputComponent->BindAction("Fire", EInputEvent::IE_Pressed, this, &ACPlayer::StartFire);
  PlayerInputComponent->BindAction("Fire", EInputEvent::IE_Released, this, &ACPlayer::StopFire);
  PlayerInputComponent->BindAction("Reload", EInputEvent::IE_Pressed, this, &ACPlayer::BeginReload);
  PlayerInputComponent->BindAction("BuyAmmo", EInputEvent::IE_Pressed, this, &ACPlayer::BuyAmmo);
}

bool ACPlayer::CanFire()
{
  return !bReloading;
}

void ACPlayer::Death()
{
  GetCharacterMovement()->DisableMovement();
  SetLifeSpan(5.0f);

  GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
  StopFire();
  if (GetMesh())
  {
    GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    GetMesh()->SetSimulatePhysics(true);
  }
}

void ACPlayer::IfHealthChanged(float Health, float Damage) {}

void ACPlayer::SetAuctionTimer()
{
  AuctionVisibility = false;
  GetWorldTimerManager().SetTimer(AuctionHandle, this, &ACPlayer::ShowAuction, //
      11.0f, true, FMath::RandRange(5.0f, 10.0f));
}

void ACPlayer::ShowAuction()
{
  if (AuctionHandle.IsValid())
    GetWorldTimerManager().ClearTimer(AuctionHandle);
  AuctionVisibility = true;
  Price = FMath::RandBool() ? 20 : 40;
  SellingAmmo = Price;
  GetWorldTimerManager().SetTimer(SellingHandle, this, &ACPlayer::PriceDrop, 1.0f, true);
}

void ACPlayer::BuyAmmo()
{
  if (!AuctionVisibility)
    return;
  if (SellingHandle.IsValid())
  {
    GetWorldTimerManager().ClearTimer(SellingHandle);
  }
  if (Money > Price)
  {
    Money = Money - Price;
    CurrentWeapon->Bullets += SellingAmmo;
  }
  NewAuction.Broadcast();
}

void ACPlayer::PriceDrop()
{
  Price--;
  if (Price <= 0)
  {
    if (SellingHandle.IsValid())
    {
      GetWorldTimerManager().ClearTimer(SellingHandle);
    }
    NewAuction.Broadcast();
  }
}

void ACPlayer::InputAxisX(float Value)
{
  AxisX = Value;
}

void ACPlayer::InputAxisY(float Value)
{
  AxisY = Value;
}

void ACPlayer::MovementTick(float DeltaTime)
{
  AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
  AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

  APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
  if (myController)
  {
    FHitResult ResultHit;
    myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);
    float RotatYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
    SetActorRotation(FQuat(FRotator(0.0f, RotatYaw, 0.0f)));
  }
}

void ACPlayer::BeginReload()
{
  if (!ReloadAnim && bReloading && !CurrentWeapon)
    return;
  if (CurrentWeapon->Bullets <= 0)
    return;
  if (CurrentWeapon->BulletsInClip == CurrentWeapon->Clip || CurrentWeapon->Bullets == CurrentWeapon->BulletsInClip)
    return;
  bReloading = true;
  PlayAnimMontage(ReloadAnim);
  GetWorldTimerManager().SetTimer(ReloadHandle, this, &ACPlayer::Reload, ReloadTime);
  CurrentWeapon->StopFire();
}

void ACPlayer::Reload()
{
  if (CurrentWeapon)
  {
    
    CurrentWeapon->BulletsInClip = UKismetMathLibrary::FMin(CurrentWeapon->Clip, CurrentWeapon->Bullets);
    bReloading = false;
  }
}
