// Fill out your copyright notice in the Description page of Project Settings.


#include "Develop/CPlayerController.h"
#include "Casoline/CasolineGameModeBase.h"

void ACPlayerController::BeginPlay()
{
  Super::BeginPlay();

  if (GetWorld())
  {
    const auto GameMode = Cast<ACasolineGameModeBase>(GetWorld()->GetAuthGameMode());
    if (GameMode)
    {
      GameMode->OnMathStateChanged.AddUObject(this, &ACPlayerController::OnMatchStateChahged);
    }
  }
}

void ACPlayerController::SetupInputComponent()
{
  Super::SetupInputComponent();
  if (!InputComponent)
    return;
  InputComponent->BindAction("Pause", EInputEvent::IE_Pressed, this, &ACPlayerController::OnPauseGame);
}

void ACPlayerController::OnPauseGame()
{
  if (!GetWorld() || !GetWorld()->GetAuthGameMode())
    return;
  GetWorld()->GetAuthGameMode()->SetPause(this);
}

void ACPlayerController::OnMatchStateChahged(EMatchState State)
{
  if (State == EMatchState::InProgress) // ���� ��������� � �������� ����
  {
    SetInputMode(FInputModeGameOnly()); // ��������� ���� ��� ������, ����� ������ ����� ���������
    bShowMouseCursor = false;           // ������ �� ����������
  }
  else // ���� � ����� ������ ���������
  {
    SetInputMode(FInputModeUIOnly()); // ��������� ��� ������, ����� ������ �����������
    bShowMouseCursor = true;          // ���������� ������
  }
}
