// Fill out your copyright notice in the Description page of Project Settings.

#include "Weapon/Projectile.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"

// Sets default values
AProjectile::AProjectile()
{
  // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = false;

  CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent")); // Create collision
  CollisionComponent->InitSphereRadius(5.0f);                                             // Projectile radius
  CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly); // Query Only (No Physics Collision) Collison Turn on
  CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block); // Projectile collision Block now
  SetRootComponent(CollisionComponent);                                                 // Set root

  MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("MovementComponent"));
  MovementComponent->UpdatedComponent = RootComponent;
  MovementComponent->InitialSpeed = 2000.0f;
  MovementComponent->ProjectileGravityScale = 0.0f;
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
  Super::BeginPlay();
  check(MovementComponent);
  check(CollisionComponent);
  check(GetWorld());

  // MovementComponent->Velocity = ShotDirection * MovementComponent->InitialSpeed;
  CollisionComponent->IgnoreActorWhenMoving(this, true);
  SetLifeSpan(LifeTime);
  CollisionComponent->OnComponentHit.AddDynamic(this, &AProjectile::OnProjectileHit);
}

AController* AProjectile::GetController() const
{
  const auto Pawn = Cast<APawn>(GetOwner());
  return Pawn ? Pawn->GetController() : nullptr;
}

void AProjectile::OnProjectileHit(
    UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
  if (!GetWorld())
    return;

  MovementComponent->StopMovementImmediately();
  Destroy();
}

/* Called every frame
void AProjectile::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);

}*/
