// Fill out your copyright notice in the Description page of Project Settings.

#include "Weapon/Weapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "Weapon/Projectile.h"
#include "Develop/CPlayer.h"

DEFINE_LOG_CATEGORY_STATIC(WeaponLog, All, All);

// Sets default values
AWeapon::AWeapon()
{
  // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
  PrimaryActorTick.bCanEverTick = false;

  WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>("WeaponMesh");
  SetRootComponent(WeaponMesh);
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
  Super::BeginPlay();
  BulletsInClip = Clip;
}

void AWeapon::MakeShot()
{
  if (!ProjectileClass && Bullets <= 0)
  {
    return;
  }

  if (BulletsInClip <= 0)
  {
    OnReload.Broadcast();
    StopFire();
    return;
  }
  FTransform Transform;
  Transform.SetLocation(WeaponMesh->GetSocketLocation(MuzzleSocketName));
  FRotator ViewRotation = WeaponMesh->GetSocketRotation(MuzzleSocketName);
  Transform.SetRotation(FQuat(ViewRotation));

  AProjectile* Projectile = GetWorld()->SpawnActor<AProjectile>(ProjectileClass, Transform);
  if (!Projectile)
    return;
  --BulletsInClip;
  --Bullets;
}

// Called every frame
void AWeapon::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);
}

void AWeapon::StartFire()
{
  /*ACPlayer* Player = Cast<ACPlayer>(GetOwner());
  if (!Player  && !Player->CanFire())
    return;*/
  GetWorldTimerManager().SetTimer(ShotTimerHandle, this, &AWeapon::MakeShot, TimeBetweenShots, true);
  MakeShot();
}

void AWeapon::StopFire()
{
  bool TimerValid = ShotTimerHandle.IsValid();
  if (TimerValid)
    GetWorldTimerManager().ClearTimer(ShotTimerHandle);
}
