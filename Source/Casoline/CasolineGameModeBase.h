// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Develop/Types.h"
#include "CasolineGameModeBase.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnMathStateChangedSignature, EMatchState);

class ASpawnerBase;

UCLASS()
class CASOLINE_API ACasolineGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
		public:
  FOnMathStateChangedSignature OnMathStateChanged;

    // ������ �� ��, ��� � BeginPlay(), ������ ���� ������
  virtual void StartPlay() override;




  protected:
  virtual void BeginPlay() override;
  // ������������� ���� � ���� �������
  virtual bool SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate = FCanUnpause()) override;

  // �������� ���� � �������
  virtual bool ClearPause() override;



		private:
  // ��������� ����. Enum �� Types
  EMatchState MatchState = EMatchState::WaitingToStart;

    // ������������� ��� ��������� ����
  void SetMatchState(EMatchState State);




};
