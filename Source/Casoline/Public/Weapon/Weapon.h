// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Weapon.generated.h"



DECLARE_MULTICAST_DELEGATE(FOnReload);

class AProjectile;

UCLASS()
class CASOLINE_API AWeapon : public AActor
{
  GENERATED_BODY()

  public:
  // Sets default values for this actor's properties
  AWeapon();


  FOnReload OnReload;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (ClampMin = 1, ClampMax = 50))
  // �������� �������� � ��������
  int32 Clip = 30;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta = (ClampMin = 1))
  // ������� � ���������
  int32 Bullets = 30;
  // ������� � ��������
  int32 BulletsInClip;


  protected:


  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon", meta = (ClampMin = "0.05", ClampMax = "3.0"))
  // ����� ����� ����������
  float TimeBetweenShots = 0.1f;

  // �������!
  void MakeShot();

  UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
  // ��� ������
  USkeletalMeshComponent* WeaponMesh;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
  // ����� ����
  FName MuzzleSocketName = "MuzzleSocket";

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
  // ����
  TSubclassOf<AProjectile> ProjectileClass = nullptr;

  public:
  // Called every frame
  virtual void Tick(float DeltaTime) override;

  void StartFire();
  void StopFire();

  private:
  FTimerHandle ShotTimerHandle;
};
