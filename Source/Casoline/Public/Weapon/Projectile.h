// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

class USphereComponent;
class UProjectileMovementComponent;
class UWeaponFXComponent;

UCLASS()
class CASOLINE_API AProjectile : public AActor
{
  GENERATED_BODY()

  public:
  // Sets default values for this actor's properties
  AProjectile();

  protected:
  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
  USphereComponent* CollisionComponent;

  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
  UProjectileMovementComponent* MovementComponent = nullptr;



  UPROPERTY(BlueprintReadWrite)
  FVector ShotDirection;

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
  float LifeTime = 5.0f;

  AController* GetController() const;

  public:
  // Called every frame
  //virtual void Tick(float DeltaTime) override;

  void SetShotDirection(const FVector& Direction) { ShotDirection = Direction; }
  float DamageRadius = 50.0f;
  float DamageAmount = 1000.0f;

  UFUNCTION()
  void OnProjectileHit(
      UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
};
