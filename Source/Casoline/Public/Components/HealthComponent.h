// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Develop/Types.h"
#include "TimerManager.h"
#include "HealthComponent.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnDeath);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnChangeHealth, float, float);

class UCameraShakeBase;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CASOLINE_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UHealthComponent();

	  FOnDeath OnDeath;
  FOnChangeHealth OnChangeHealth;

protected:

	virtual void BeginPlay() override;

	  private:
  bool bIsDead = false;
  float Health = 0.0f;

  UFUNCTION()
  void AnyDamage(
      AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

  //  REGEN, HEAL
  void Regeneration();
  FTimerHandle RegenHandle;
  bool bRegenFlag = false;
  float RegenTime = 0.0f;
  UPROPERTY(EditDefaultsOnly, Category = "Heal")
  bool bAutoHeal = true;
  UPROPERTY(EditDefaultsOnly, Category = "Heal", meta = (ClampMin = "0.016", ClampMax = "180", EditCondition = "bAutoHeal"))
  float HealUpdateTime = 0.5f;
  UPROPERTY(EditDefaultsOnly, Category = "Heal", meta = (ClampMin = "0.0", ClampMax = "180", EditCondition = "bAutoHeal"))
  float HealDelay = 10.0f;
  UPROPERTY(EditDefaultsOnly, Category = "Heal", meta = (ClampMin = "0.0", ClampMax = "1000000000", EditCondition = "bAutoHeal"))
  float HealRegen = 2;
  bool TimerManagerValid = false;

  void PlayCameraShake();



  UPROPERTY()
  //
  AController* KillerContr = nullptr;

    // ====== PUBLIC ======
  public:
  UFUNCTION(BlueprintCallable)
  float GetHealth() const { return Health; }

  UFUNCTION(BlueprintCallable)
  float GetHealthPercent() const { return Health / MaxHealth; }

  UFUNCTION(BlueprintCallable)
  bool GetIsDead() const { return bIsDead; }

  void ChangeHealth(float Damage);

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health", meta = (ClampMin = "1.0", ClampMax = "999999.9"))
  float MaxHealth = 100.0f;

  bool RegenPercent(float Percent);

  protected:
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
  TSubclassOf<UCameraShakeBase> CameraShake;
};
