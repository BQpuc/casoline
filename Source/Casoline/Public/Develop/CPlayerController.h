// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Develop/Types.h"
#include "CPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CASOLINE_API ACPlayerController : public APlayerController
{
	GENERATED_BODY()

  protected:
  virtual void BeginPlay() override;

  // ����� ���������� �������� ������ � �������
  virtual void SetupInputComponent() override;

  private:
  // ����� ����� ��� ������� ������ "P"
  void OnPauseGame();

  // ���������� ��� ��������� ��������� ����. ����������/�������� ������
  void OnMatchStateChahged(EMatchState State);
};
