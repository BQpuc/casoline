// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "UObject/NameTypes.h"
#include "CGameInstance.generated.h"



UCLASS()
class CASOLINE_API UCGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
		public:
  // �������� ��� ���������� ������
  FName GetStartupLvlName() const { return StartupLevelName; }
  // �������� ��� ������ �������� ����
  FName GetMenuLvlName() const { return MenuLevelName; }

  protected:
  UPROPERTY(EditDefaultsOnly, Category = "Game")
  // ��� ���������� ������
  FName StartupLevelName = NAME_None;

  UPROPERTY(EditDefaultsOnly, Category = "Game")
  // ��� ������ �������� ����
  FName MenuLevelName = NAME_None;
};
