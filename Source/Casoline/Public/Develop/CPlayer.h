// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CPlayer.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnNewAuction);

class AWeapon;
class UHealthComponent;
class ASpawnerBase;

UCLASS()
class CASOLINE_API ACPlayer : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACPlayer();

	FOnNewAuction NewAuction;

  protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
  // ������
  TSubclassOf<AWeapon> WeaponClass = nullptr;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
  // ��� ������ ��� ������
  FName WeaponSocketName = "WeaponSocket";

  UPROPERTY()
  AWeapon* CurrentWeapon;

  UFUNCTION()
  void StartFire();
  UFUNCTION()
  void StopFire();

  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
  UAnimMontage* ReloadAnim = nullptr;
  UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
  float ReloadTime = 2.83f;
  FTimerHandle ReloadHandle;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
  TSubclassOf<ASpawnerBase> SpawnerClass;

	public:
    UPROPERTY()
    ASpawnerBase* Spawner = nullptr;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
  FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
  /** Returns CameraBoom subobject **/
  FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
   UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
  UHealthComponent* HealthComponent = nullptr;

  private:
  /** Top down camera */
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
  class UCameraComponent* TopDownCameraComponent;

  /** Camera boom positioning the camera above the character */
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
  class USpringArmComponent* CameraBoom;

  void SetAuctionTimer();
  void ShowAuction();
  void BuyAmmo();
  void PriceDrop();
  FTimerHandle AuctionHandle;
  FTimerHandle SellingHandle;


  public:
  // cursor
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
  UMaterialInterface* CursorMaterial = nullptr;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
  FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

  	UDecalComponent* CurrentCursor = nullptr;
  // Inputs
  UFUNCTION()
  void InputAxisX(float Value);
  UFUNCTION()
  void InputAxisY(float Value);


  float AxisX = 0.0f;
  float AxisY = 0.0f;

  	// tick Func
  UFUNCTION()
  void MovementTick(float DeltaTime);

  AWeapon* GetWeapon() const { return CurrentWeapon; }

  // ��������� �������� � ������ �����������
  void BeginReload();
  // ��������� ������� � �������
  void Reload();
  // ����� ��������. ���� ������ ��������������, �� ������
  bool CanFire();
  // � �������� �����������?
  bool bReloading = false;

  void Death();
  void IfHealthChanged(float Health, float Damage);
  UPROPERTY(EditAnywhere, BlueprintReadWrite)
  bool PlayerInRadius = false;

  // ������
  UPROPERTY(BlueprintReadWrite)
  int32 Money;
  UPROPERTY(BlueprintReadWrite)
  int32 Price = 0;
  UPROPERTY(BlueprintReadWrite)
  int32 SellingAmmo;
  UPROPERTY(BlueprintReadWrite)
  bool AuctionVisibility = false;

};
