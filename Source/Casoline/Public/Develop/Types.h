// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "UObject/NameTypes.h"
#include "Animation/AnimMontage.h"
#include "Animation/AnimSequenceBase.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMatchState : uint8
{
  WaitingToStart = 0,
  InProgress,
  Pause,
  GameOver
};

UCLASS()
class CASOLINE_API UTypes : public UBlueprintFunctionLibrary
{
  GENERATED_BODY()

  public:

  template <typename T> static T* GetPlayerComponent(AActor* Pawn)
  {
    if (!Pawn)
      return nullptr;
    const auto Component = Pawn->GetComponentByClass(T::StaticClass());
    return Cast<T>(Component);
  }
};
