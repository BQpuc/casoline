// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Develop/Types.h"
#include "GameWidget.generated.h"

class ACasolineGameModeBase;
class ACPlayer;
class AWeapon;

UCLASS()
class CASOLINE_API UGameWidget : public UUserWidget
{
  GENERATED_BODY()

  public:
  UFUNCTION(BlueprintCallable)
  int32 GetBullets() const;

  UFUNCTION(BlueprintCallable)
  int32 GetBulletsInClip() const;

  UFUNCTION(BlueprintCallable)
  int32 GetClip() const;

  UFUNCTION(BlueprintCallable)
  float GetHealthPercent() const;

  UFUNCTION(BlueprintCallable)
  int32 GetWave() const;

  UFUNCTION(BlueprintCallable)
  int32 GetMoney() const;

  UFUNCTION(BlueprintCallable)
  bool GetInRadius() const;

  UFUNCTION(BlueprintCallable)
  bool GetAuctionVisibility() const;

  UFUNCTION(BlueprintCallable)
  int32 GetPrice() const;

  UFUNCTION(BlueprintCallable)
  int32 GetSellingAmmo() const;
};
