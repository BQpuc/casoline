// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Develop/Types.h"
#include "GameHUD.generated.h"


UCLASS()
class CASOLINE_API AGameHUD : public AHUD
{
	GENERATED_BODY()
	
  protected:
  virtual void BeginPlay() override;

  UPROPERTY(EditDefaultsOnly, Category = "UI")
  TSubclassOf<UUserWidget> PlayerWidget;

  UPROPERTY(EditDefaultsOnly, Category = "UI")
  TSubclassOf<UUserWidget> PauseWidget;

  UPROPERTY(EditDefaultsOnly, Category = "UI")
  TSubclassOf<UUserWidget> GameOverWidget;

  private:
  UPROPERTY()
  // ������ ��� �������� ���� ��������, ���� EMatchState, �������� UUserWidget*
  TMap<EMatchState, UUserWidget*> GameWidgets;

  UPROPERTY()
  // ��������� �� ������� ������
  UUserWidget* CurrentWidget = nullptr;

  void OnMatchStateChahged(EMatchState State);
};
