// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MenuWidget.generated.h"

class UButton;

UCLASS()
class CASOLINE_API UMenuWidget : public UUserWidget
{
	GENERATED_BODY()
	
		  protected:
  virtual void NativeOnInitialized() override;

  UPROPERTY(meta = (BindWidget))
  // ������ �������� �������� ������
  UButton* StartGameButton;

  UPROPERTY(meta = (BindWidget))
  // ������ ������ �� ������� ����
  UButton* QuitGameButton;

  private:
  UFUNCTION()
  // call back for Start game
  void OnStartGame();

  UFUNCTION()
  // Call back for Quit game
  void OnQuitGame();
};
