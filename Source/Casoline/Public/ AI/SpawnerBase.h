// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnerBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWaveSignature, int32, CurrentWave);

class AAICharacter;

UCLASS()
class CASOLINE_API ASpawnerBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnerBase();

	FOnWaveSignature OnWave;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
  int32 Wave = 0;

UFUNCTION(BlueprintCallable)
  void SpawnBots();
FTimerHandle SpawnHandle;
  UFUNCTION(BlueprintCallable)
void SpawnOneBot(TSubclassOf<AAICharacter> Bot, FVector SpawnLocation);
  UPROPERTY(EditAnywhere, BlueprintReadWrite)
  TSubclassOf<AAICharacter> Bot1;
  UPROPERTY(EditAnywhere, BlueprintReadWrite)
  TSubclassOf<AAICharacter> Bot2;
  UPROPERTY(EditAnywhere, BlueprintReadWrite)
  TSubclassOf<AAICharacter> Bot3;

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
  FVector SpawnLoc1 = FVector(0.0f, 0.0f, 0.0f);
  UPROPERTY(EditAnywhere, BlueprintReadWrite)
  FVector SpawnLoc2 = FVector(0.0f, 0.0f, 0.0f);
  UPROPERTY(EditAnywhere, BlueprintReadWrite)
  FVector SpawnLoc3 = FVector(0.0f, 0.0f, 0.0f);

  UPROPERTY(EditAnywhere, BlueprintReadWrite)
  int32 MaxMobsDefault = 4;

  int32 MaxMob1 = 1;
  int32 MaxMob2 = 1;
  int32 MaxMob3 = 1;

  UFUNCTION()
  void WaveTimer(int32 myWave);

};
