// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AICharacter.generated.h"

class UHealthComponent;
class USphereComponent;

UCLASS()
class CASOLINE_API AAICharacter : public ACharacter
{
  GENERATED_BODY()

  public:
  // Sets default values for this character's properties
  AAICharacter();
  UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
  UHealthComponent* HealthComponent = nullptr;

  protected:
  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0.0", ClampMax = "1.0"))
  float Veroyatnost = 0.5f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite)
  TSubclassOf<AActor> Pickup;

  public:
  // Called every frame
  virtual void Tick(float DeltaTime) override;

  // Called to bind functionality to input
  virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

  void Death();
  FTimerHandle DamageHandle;
  void DamageSphere();

  UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0.5", ClampMax = "3.0"))
  // ������ ����
  FVector Scale = FVector(0.5f);
  UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "100.0", ClampMax = "600.0"))
  // �������� ������������
  float Speed = 300.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "100.", ClampMax = "500.0"))
  // ������ ����� �������� ���� / ������ ������������ �����
  float Radius1 = 300.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0.5", ClampMax = "30.0"))
  float Damage = 10.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0.5", ClampMax = "2999.0"))
  float HP = 990.0f;
  UPROPERTY(EditAnywhere, BlueprintReadWrite)
  bool IsBoss = false;
  UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "500.0", ClampMax = "900.0", EditCondition = "IsBoss"))
  // ������ ����� �����
  float Radius2 = 500.0f;
  UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
  // ����� �� ���������� �������. ������������ ���� ����� ������ �����-��������
  bool PlayerInRadius = false;
};
