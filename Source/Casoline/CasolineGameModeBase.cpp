// Copyright Epic Games, Inc. All Rights Reserved.


#include "CasolineGameModeBase.h"
#include "Develop/CPlayer.h"


void ACasolineGameModeBase::StartPlay()
{
  Super::StartPlay();

  SetMatchState(EMatchState::InProgress);
}

void ACasolineGameModeBase::BeginPlay()
{
  Super::BeginPlay();
}

bool ACasolineGameModeBase::SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate)
{
  bool Pause = Super::SetPause(PC, CanUnpauseDelegate);
  if (Pause)
  {
    SetMatchState(EMatchState::Pause);
  }
  return Pause;
}

bool ACasolineGameModeBase::ClearPause()
{
  bool PauseCleared = Super::ClearPause();
  if (PauseCleared)
  {
    SetMatchState(EMatchState::InProgress);
  }
  return PauseCleared;
}

void ACasolineGameModeBase::SetMatchState(EMatchState State)
{
  if (MatchState == State)
    return;

  MatchState = State;
  OnMathStateChanged.Broadcast(MatchState);
}


